# BelVG Import Product Api Extension

Magento 2 extension to import products via API 

## Installation

 * Enable module with `php bin/magento module:enable BelVG_ImportProductApi`
 * Run `php bin/magento setup:upgrade`
 * Recompile DI `php bin/magento setup:di:compile`
 * Recompile static files: `php bin/magento setup:static-content:deploy`
 * Flush cache `php bin/magento cache:flush`

## Usage

Request:
```
{
	"data": {
		"options":{
			"entity":"catalog_product_api",
			"behavior": "append",
			"validation_strategy":"validation-skip-errors",
			"allowed_error_count": 10
		},
		"products": [
	  {
	    "sku": "24-MB01",
	    "store_view_code": "",
	    "attribute_set_code": "Bag",
	    "product_type": "simple",
	    "categories": "Default Category/Gear,Default Category/Gear/Bags",
	    "product_websites": "base",
	    "name": "Joust Duffle Bag",
	    "description": "<p>The sporty Joust Duffle Bag can't be beat - not in the gym, not on the luggage carousel, not anywhere. Big enough to haul a basketball or soccer ball and some sneakers with plenty of room to spare, it's ideal for athletes with places to go.<p>\n<ul>\n<li>Dual top handles.</li>\n<li>Adjustable shoulder strap.</li>\n<li>Full-length zipper.</li>\n<li>L 29\" x W 13\" x H 11\".</li>\n</ul>",
	    "short_description": "",
	    "weight": "",
	    "product_online": 1,
	    "tax_class_name": "",
	    "visibility": "Catalog, Search",
	    "price": 34,
	    "special_price": "",
	    "special_price_from_date": "",
	    "special_price_to_date": "",
	    "url_key": "joust-duffle-bag",
	    "meta_title": "",
	    "meta_keywords": "",
	    "meta_description": "",
	    "base_image": "/m/b/mb01-blue-0.jpg",
	    "base_image_label": "",
	    "small_image": "/m/b/mb01-blue-0.jpg",
	    "small_image_label": "",
	    "thumbnail_image": "/m/b/mb01-blue-0.jpg",
	    "thumbnail_image_label": "",
	    "swatch_image": "",
	    "swatch_image_label": "",
	    ...
	  },
	  ...
	]
	}
}
```
Response:
```
[
    {
        "messages": [
            "Import successfully done"
        ],
        "created_items": 0,
        "updated_items": 4,
        "deleted_items": 1,
        "processed_entities": 8,
        "processed_rows": 4
    }
]
```
## Support

Get in touch with us: https://belvg.com/support

## Demo

http://magento2-import-product-api.deploy.belvg.net/

