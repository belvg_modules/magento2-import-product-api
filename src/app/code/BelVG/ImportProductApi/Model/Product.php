<?php

namespace BelVG\ImportProductApi\Model;

use BelVG\ImportProductApi\Api\ProductInterface;
use BelVG\ImportProductApi\Model\Import\Adapter;

use \Magento\ImportExport\Model\Import as MagentoImport;
use \Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingError;
use \Magento\ImportExport\Model\Import\ErrorProcessing\ProcessingErrorAggregatorInterface;

/**
 * Class Product
 * @package BelVG\ImportProductApi\Model
 */
class Product implements ProductInterface {

    /**
     * @var \Magento\ImportExport\Model\Import
     */
    protected $importModel;

    /** @var array */
    protected $report = [];

    /** @var Import\Source\Api  */
    protected $sourceModel;


    public function __construct(
        MagentoImport $importModel
    ) {
        $this->importModel = $importModel;
    }

    /**
     * @inheritdoc
     */
    public function import($data = []) {
        $this->importModel->setData(array_merge($this->_getDefaultDataset(), $data['options']));
        $source = $this->getSource($data);

        try{
           $this->importModel->validateSource($source);
        } catch (\Exception $e) {

        }

        try{
            $this->importModel->importSource();
        } catch (\Exception $e) {

        }

        $this->handleInvalidate();
        return [$this->getReport()];
    }

    /** Get default import options
     * @return array
     */
    private function _getDefaultDataset() {
        return [
            'entity' => 'catalog_product',
            'behavior' => 'append',
            MagentoImport::FIELD_NAME_VALIDATION_STRATEGY => ProcessingErrorAggregatorInterface::VALIDATION_STRATEGY_SKIP_ERRORS,
            'allowed_error_count' => '10',
            '_import_field_separator' => ',',
            '_import_multiple_value_separator' => ','
        ];
    }

    /**
     * Creating API adapter for import
     *
     * @param array $data
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \BelVG\ImportProductApi\Model\Import\Adapter|\Magento\ImportExport\Model\Import\AbstractSource
     */
    protected function getSource($data) {
        return Adapter::findAdapterFor('api', $data['products'], null);
    }

    /** Creates report if needed and return it */
    protected function getReport() {
         if (!$this->report) {
             $errorAggregator = $this->importModel->getErrorAggregator();
             if ($errorAggregator->hasToBeTerminated()) {
                 $this->report['messages'][] = __('Import finished with errors');
                 if ($errorAggregator->hasFatalExceptions()) {
                     foreach ($errorAggregator->getAllErrors() as $error) {
                         if ($error->getErrorLevel() == ProcessingError::ERROR_LEVEL_CRITICAL ) {
                             $error =  $error->getRowNumber().': '. $error->getErrorCode() . ' -  ' .$error->getErrorMessage();
                             $this->report['errors'][] = $error;
                         }
                     }
                 } else {
                     $this->report['errors'][] = __('Maximum error count has been reached or system error is occurred!');
                 }
             } else {
                 $this->report['messages'][] = __('Import successfully done');
             }

             $this->report['created_items'] = $this->importModel->getCreatedItemsCount();
             $this->report['updated_items'] = $this->importModel->getUpdatedItemsCount();
             $this->report['deleted_items'] = $this->importModel->getDeletedItemsCount();
             $this->report['processed_entities'] = $this->importModel->getProcessedEntitiesCount();
             $this->report['processed_rows'] = $this->importModel->getProcessedRowsCount();
         }
         return $this->report;
    }

    /** Invalidates indexes if no import errors
     * @return void
     */
    protected function handleInvalidate() {
        $report = $this->getReport();
        if (!isset($report['errors']) || !$report['errors']) {
            $this->importModel->invalidateIndex();
        }
    }
}